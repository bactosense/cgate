/*

cgate
---

An polynomial gating extension for Python

(c) 2015, Douglas C. Watson <douglas.watson@epfl.ch>
(c) 2021, Adrian Shajkofci

License: GNU GPL.

*/

#include <Python.h>
// #include <numpy/arrayobject.h>

static char module_docstring[] =
    "cgate: fast polygonal gating of points.\n\n"
    "Gate a single point with `in_poly`, gate a list of points with "
    "`in_poly_multiple`.\n";

/* ------------------------------------------
 * PURE C FUNCTIONS
 * ------------------------------------------ */

/*

in_gate returns true if point (x0, y0) is containted inside the polygon of n
points, traced by the vertices of coordinates (x[i], y[i]).

Do not repeat the "joining" point of the polygon, i.e. a square has n = 4, not
5.

bb is the bounding box, the smallest rectangle that contains all points of the
polygon. Used to speed up the algorithm:

    double bb[4] = {xmin, xmax, ymin, ymax};

*/
int
in_gate(double x0, double y0, double *x, double *y, int n, double *bb)
{
    // Discard if outside of bounding box
    if ( (x0 < bb[0]) || (x0 > bb[1]) || (y0 < bb[2]) || (y0 > bb[3]) ) {
        return 0;
    }

    // Ray trace to the right
    int i, j, c = 0;
    for (i = 0, j = n-1; i < n; j = i++) {
        if (
            ( (y[i] > y0) != (y[j] > y0) ) &&
            ( x0 < (x[j]-x[i]) * (y0 - y[i]) / (y[j] - y[i]) + x[i])
        ) {
            c = !c;
        }
    }
    return c;
}

/*
in_gate_multiple: gate all N points listed as coordinates xs, ys.

Stores results in res (int [])


*/
void
in_gate_multiple(double *xs, double *ys, char *res, int N,
                 double *x, double *y, int n, double *bb)
{
    int i;
    for ( i = 0; i < N; i++ ) {
        res[i] = (char) in_gate(xs[i], ys[i], x, y, n, bb);
    }
}


/* ------------------------------------------
 * PYTHON WRAPPERS (AND HELPERS)
 * ------------------------------------------ */

/* Parse Python list object, return pointer to newly allocated
   array and set len to length of the new array.

*/
double *
parse_list(PyObject * listobj, unsigned int *len)
{
    PyObject *seq;      // Sequence extracted from that list
    PyObject *item;     // Item of the sequence
    double *wbuf;         // C array to transfer the values to

    seq = PySequence_Fast(listobj, "argument must be a list");
    (*len) = PySequence_Fast_GET_SIZE(seq);

    // Allocate C array and populate it
    wbuf = malloc(*len * sizeof(*wbuf));
    if ( wbuf == NULL ) {
        Py_XDECREF(seq);
        PyErr_NoMemory();
        return NULL;
    }

    long int i;
    double tmp;
    // Shift indexing if we need to insert address at beginning
    for ( i = 0 ; i < *len ; i++ ) {
        item = PySequence_Fast_GET_ITEM(seq, i);

        tmp = PyFloat_AsDouble(item);
        if ( tmp == -1.0  && PyErr_Occurred() != NULL ) {
            // printf("Float to double conversion failed (index %ld)", i);
            Py_XDECREF(seq);
            free(wbuf);
            return NULL;
        }

        wbuf[i] = (double) tmp;
    }

    Py_XDECREF(seq);
    return wbuf;
}

PyObject *
build_list(char * carray, unsigned int len)
{
    PyObject * pylist = PyList_New(len);

    int i;
    for ( i = 0 ; i < len ; i++ ) {
        PyList_SET_ITEM(pylist, i, Py_BuildValue("i", (int)carray[i]));
    }

    return pylist;
}


static char in_poly_docstring[] =
    "in_poly(x0, y0, x, y, bb) -> bool\n\n"
    "Return True if point (x0, y0) is inside the polygon defined by the points\n"
    "in x and y.\n\n"
    "bb defines the Bounding Box of the polynomial: (xmin, xmax, ymin, ymax).\n";

static PyObject *
cgate_in_poly(PyObject *self, PyObject *args)
{
    double x0, y0;       // Point coordinates

    unsigned int nx;    // Number of x coordinates in polygon
    unsigned int ny;    // Number of y coordinates in polygon
    unsigned int nbb;   // Number of points in bounding box
    double *x;          // Array of x coordinates of polygon
    double *y;          // Array of y coordinates of polygon
    double *bb;         // Bounding box of polygon
    PyObject *xobj;     // List of x coords, before conversion
    PyObject *yobj;     // List of y coords, before conversion
    PyObject *bbobj;    // List of coords in bb, before conversion

    int res;  // Result of gating

    // double x[4] = {4.0, 4.0, 6.0, 6.0};
    // double y[4] = {0.0, 4.0, 6.3, 0.0};

    if (!PyArg_ParseTuple(args, "ddOOO", &x0, &y0, &xobj, &yobj, &bbobj))
        return NULL;
    x = parse_list(xobj, &nx);
    y = parse_list(yobj, &ny);
    bb = parse_list(bbobj, &nbb);
    if ( x == NULL ) {
        PyErr_SetString(PyExc_TypeError,
                        "`x` argument must be a list of float."
                       );
        return NULL;
    }
    if ( y == NULL ) {
        PyErr_SetString(PyExc_TypeError,
                        "`y` argument must be a list of float."
                       );
        return NULL;
    }
    if ( bb == NULL ) {
        PyErr_SetString(PyExc_TypeError,
                        "`bb` should be a list of floats."
                       );
        return NULL;
    }
    if ( nx != ny ) {
        PyErr_SetString(PyExc_TypeError,
                        "length of `x` and `y` must be equal."
                       );
        free(x);
        free(y);
        free(bb);
        return NULL;
    }
    if ( nbb != 4 ) {
        PyErr_SetString(PyExc_TypeError,
                        "`bb` must be of length 4."
                       );
        free(x);
        free(y);
        free(bb);
        return NULL;

    }

    res = in_gate(x0, y0, x, y, nx, bb);

    free(x);
    free(y);
    free(bb);
    return Py_BuildValue("b", res);
}

static char in_poly_multiple_docstring[] =
    "in_poly_multiple(xs, ys, x, y, bb) -> List of bool\n\n"
    "Return list of length len(xs). Item i is true if point (xs[i], ys[i])\n"
    "is in the polygon defined by point pairs (x[k], y[k]).\n\n"
    "bb defines the Bounding Box of the polynomial: (xmin, xmax, ymin, ymax).\n";

static PyObject *
cgate_in_poly_multiple(PyObject *self, PyObject *args)
{
    double *xs;         // x coords of points to test
    double *ys;         // y coords of points to test
    char *res;        // Result of gating: array of 1 or 0
    unsigned int Nx;    // Number of points in xs
    unsigned int Ny;    // Number of points in ys
    PyObject *xsobj;    // List of x coords to test, before conversion
    PyObject *ysobj;    // List of y coords to test, before conversion

    unsigned int nx;    // Number of x coordinates in polygon
    unsigned int ny;    // Number of y coordinates in polygon
    unsigned int nbb;   // Number of points in bounding box
    double *x;          // Array of x coordinates of polygon
    double *y;          // Array of y coordinates of polygon
    double *bb;         // Bounding box of polygon
    PyObject *xobj;     // List of polygon x coords, before conversion
    PyObject *yobj;     // List of polygon y coords, before conversion
    PyObject *bbobj;    // List of coords in bb, before conversion

    PyObject *retval;   // Return value: list of bools

    // double x[4] = {4.0, 4.0, 6.0, 6.0};
    // double y[4] = {0.0, 4.0, 6.3, 0.0};

    if (!PyArg_ParseTuple(args, "OOOOO", &xsobj, &ysobj, &xobj, &yobj, &bbobj))
        return NULL;
    xs = parse_list(xsobj, &Nx);
    if ( xs == NULL ) {
        PyErr_SetString(PyExc_TypeError,
                        "`xs` argument must be a list of float."
                       );
        return NULL;
    }
    ys = parse_list(ysobj, &Ny);
    if ( ys == NULL ) {
        PyErr_SetString(PyExc_TypeError,
                        "`ys` argument must be a list of float."
                       );
        free(xs);
        return NULL;
    }
    x = parse_list(xobj, &nx);
    if ( x == NULL ) {
        PyErr_SetString(PyExc_TypeError,
                        "`x` argument must be a list of float."
                       );
        free(xs);
        free(ys);
        return NULL;
    }
    y = parse_list(yobj, &ny);
    if ( y == NULL ) {
        PyErr_SetString(PyExc_TypeError,
                        "`y` argument must be a list of float."
                       );
        free(xs);
        free(ys);
        free(x);
        return NULL;
    }
    bb = parse_list(bbobj, &nbb);
    if ( bb == NULL ) {
        PyErr_SetString(PyExc_TypeError,
                        "`bb` should be a list of floats."
                       );
        free(xs);
        free(ys);
        free(x);
        free(y);
        return NULL;
    }
    if ( Nx != Ny ) {
        PyErr_SetString(PyExc_TypeError,
                        "length of `xs` and `ys` must be equal."
                       );
        free(xs);
        free(ys);
        free(x);
        free(y);
        free(bb);
        return NULL;
    }
    if ( nx != ny ) {
        PyErr_SetString(PyExc_TypeError,
                        "length of `x` and `y` must be equal."
                       );
        free(xs);
        free(ys);
        free(x);
        free(y);
        free(bb);
        return NULL;
    }
    if ( nbb != 4 ) {
        PyErr_SetString(PyExc_TypeError,
                        "`bb` must be of length 4."
                       );
        free(xs);
        free(ys);
        free(x);
        free(y);
        free(bb);
        return NULL;

    }


    res = (char*) malloc(Nx * sizeof(*res));
    if ( res == NULL ) {
        PyErr_NoMemory();
        free(xs);
        free(ys);
        free(x);
        free(y);
        free(bb);
        return NULL;
    }


    in_gate_multiple(xs, ys, res, Nx, x, y, nx, bb);
    retval = build_list(res, Nx);

    free(xs);
    free(ys);
    free(x);
    free(y);
    free(bb);
    free(res);

    return retval;
}

static PyMethodDef CGateMethods[] = {
    {"in_poly",  cgate_in_poly, METH_VARARGS, in_poly_docstring},
    {
        "in_poly_multiple",  cgate_in_poly_multiple, METH_VARARGS,
        in_poly_multiple_docstring
    },
    {NULL, NULL, 0, NULL}
};

PyMODINIT_FUNC
#if PY_MAJOR_VERSION >= 3
PyInit_cgate(void)
#else
initcgate(void)
#endif
{
    #if PY_MAJOR_VERSION >= 3
        static struct PyModuleDef moduledef = {
            PyModuleDef_HEAD_INIT,
            "cgate",     /* m_name */
            "cgatemodule",  /* m_doc */
            -1,                  /* m_size */
            CGateMethods,    /* m_methods */
            NULL,                /* m_reload */
            NULL,                /* m_traverse */
            NULL,                /* m_clear */
            NULL,                /* m_free */
        };
    #endif

    #if PY_MAJOR_VERSION >= 3
        PyObject *m = PyModule_Create(&moduledef);
    #else
        PyObject *m = Py_InitModule3("cgate", CGateMethods, module_docstring);

        if (m == NULL)
            return;
        return m;
    #endif

    if (m == NULL)
        return;

    /* Load `numpy` functionality. */
    // import_array();
}


