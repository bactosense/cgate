#!/usr/bin/env python
# -*- coding: UTF8 -*-
'''
cgate
~~~~~

A fast polygonal gating extension.

:author: Douglas Watson <douglas.watson@epfl.ch>
:date: 2015
:license: Private.

'''

from distutils.core import setup, Extension

setup(
    name="cgate",
    version="0.1.0",
    description="A fast polygonal gating extension, written in C.",
    author="Douglas C. Watson",
    author_email="douglas.watson@epfl.ch",
    license="Private",
    ext_modules=[
        Extension("cgate", ["cgatemodule.c"])
    ]
)
