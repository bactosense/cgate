#!/usr/bin/env python
# -*- coding: UTF8 -*-
"""
memleak.py
~~~~~~~~~~

Stress the gating procedure to attempt to replicate a memory leak
(present in the code since 2014, but only a problem for the autosampler.)

11 Jan 2019: this can run for hours, never filling up the memory.
Memory usage fluctuates between 5 and 10%, 

:author: Douglas Watson <douglas.watson@bnovate.com>
:date: 2019
:license: Private code, see LICENSE for more information

"""

import cgate
import numpy as np

from memory_profiler import profile

@profile
def stress(n=10000):

    for i in range(n):
        # Xs and Ys are x and y coords of the events
        xs = np.random.randn(1000000)
        ys = np.random.randn(1000000)

        # Xp and Yp are the coords of the polygon
        xp = [-4,  5,  4, -5]
        yp = [ 4,  4, -4, -4]

        # bb is a bounding box, we'll make it equal to Xp and Yp since they are 
        bb = (-5,  5, -4,  4)

        cgate.in_poly_multiple(xs, ys, xp, yp, bb)


if __name__ == "__main__":
    stress(500)